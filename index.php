<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'QRCodeGenerator.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = QRCodeGenerator::loadLanguages();
	QRCodeGenerator::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo QRCodeGenerator::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', QRCodeGenerator::showMessage('default input'))); ?>";
			function ajaxRequest() {
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/QRCodeGenerator/api.php',
					data: {
						'localization': '<?php echo $lang; ?>',
						'url': $('input#inputTextId').val()
					},
					success: function(msg){
						var result = jQuery.parseJSON(msg);
						var output = '';
						output += '<img src="' + result.resultUrl + '"><br /><br />';
						output += '<h4><a target="_blank" href="' + result.resultFilepath + '" download="' + result.resultFilename + '.png"><?php echo QRCodeGenerator::showMessage('download'); ?></a></h4>';
						$('#resultId').html(output);
					},
					error: function(){
						$('#resultId').html('ERROR');
					}
				});
			}
			$(document).ready(function () {
				$(document).on('click', 'button#MainButtonId', function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					ajaxRequest();
				});
				$(document).on('keypress', 'input#inputTextId', function (e) {
					if (e.which == 13) {
						$('#resultBlockId').show('slow');
						$('#resultId').empty();
						$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
						ajaxRequest();
						return false;
					}
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/QRCodeGenerator/?lang=<?php echo $lang; ?>"><?php echo QRCodeGenerator::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo QRCodeGenerator::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo QRCodeGenerator::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = QRCodeGenerator::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . QRCodeGenerator::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo QRCodeGenerator::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo QRCodeGenerator::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo QRCodeGenerator::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p>
									<input type="text" class="form-control" id="inputTextId" name="inputText"  size="100" value="<?php echo str_replace('\n', "\n", QRCodeGenerator::showMessage('default input')); ?>">
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo QRCodeGenerator::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo QRCodeGenerator::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo QRCodeGenerator::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/QRCodeGenerator" target="_blank"><?php echo QRCodeGenerator::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/QRCodeGenerator/-/issues/new" target="_blank"><?php echo QRCodeGenerator::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo QRCodeGenerator::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo QRCodeGenerator::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo QRCodeGenerator::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php QRCodeGenerator::sendErrorList($lang); ?>