<?php
	class QRCodeGenerator {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $url = '';
		private $resultUrl = '';
		private $resultFilename = '';
		private $resultFilepath = '';
		const BR = "<br>\n";

		function __construct() {

		}
		
		public function setUrl($url) {
			$this->url=$url;
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = 'lang/' . $lang . '.txt';
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = '';
					$value = '';
				}
				elseif(substr($line, 0, 1) != '#' && substr($line, 0, 2) != '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList() {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'QR Code Generator';
				$recipient = 'corpus.by@gmail.com';
				$subject = "$sendersName from IP $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/QRCodeGenerator/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function run() {
			$size = 150;
			$EC_level = 'L';
			$margin = '0';
			$this->resultUrl = 'http://chart.apis.google.com/chart?chs=' . $size . 'x' . $size . '&cht=qr&chld=' . $EC_level . '|' . $margin . '&chl=' . urlencode($this->url);
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'QRCodeGenerator';
			$sendersName = 'QR Code Generator';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->url);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$mailBody .= "URL: " . $cacheText . self::BR . self::BR;
			$mailBody .= "URL захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=$serviceName&t=in&f=$filename" . self::BR . self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.png';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			file_put_contents($filepath, file_get_contents($this->resultUrl));
			$mailBody .= "Выніковы QR-код захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=$serviceName&t=out&f=$filename" . self::BR . self::BR;
			$mailBody .= self::BR;
			$this->resultFilename = $filename;
			$this->resultFilepath = '../_cache/QRCodeGenerator/out/' . $filename;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResultFilename() {
			return $this->resultFilename;
		}
		
		public function getResultFilepath() {
			return $this->resultFilepath;
		}
		
		public function getResultUrl() {
			return $this->resultUrl;
		}
	}
?>