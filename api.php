<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$url = isset($_POST['url']) ? $_POST['url'] : '';
	
	include_once 'QRCodeGenerator.php';
	QRCodeGenerator::loadLocalization($localization);
	
	$msg = '';
	if(!empty($url)) {
		$QRCodeGenerator = new QRCodeGenerator();
		$QRCodeGenerator->setUrl($url);
		$QRCodeGenerator->run();
		$QRCodeGenerator->saveCacheFiles();
		
		$result['url'] = $url;
		$result['resultUrl'] = $QRCodeGenerator->getResultUrl();
		$result['resultFilepath'] = $QRCodeGenerator->getResultFilepath();
		$result['resultFilename'] = $QRCodeGenerator->getResultFilename();
		$msg = json_encode($result);
	}
	echo $msg;
?>