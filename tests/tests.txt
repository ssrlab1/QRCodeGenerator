######################
##### TEST TEMPLATE #####
######################
# Activation
# input: URL
# output: Output file name

######################
###### REAL TESTS #######
######################
# Activation
1
# input: URL
https://corpus.by/
# output: Output file name
corpus.png

# 1+
# Activation
1
# input: URL
https://be.wikipedia.org/
# output: Output file name
wikipedia.png

# 2+
# Activation
1
# input: URL
http://cooks.org.kp
# output: Output file name
north_korea.png

# 3+
# Activation
1
# input: URL
https://www.multitran.com/m.exe?l1=2&l2=1&s=%D0%BC%D0%B8%D0%BD%D0%B5%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F%20%D0%B2%D0%BE%D0%B4%D0%B0
# output: Output file name
multitran.png

# 4+
# Activation
1
# input: URL
https://ssrlab.by/fajnyja-spasylki
# output: Output file name
ssrlab.png

# 5+
# Activation
1
# input: URL
https://www.google.com/
# output: Output file name
google.png

# 5+
# Activation
1
# input: URL
https://corpus.by/QRCodeGenerator/?lang=en
# output: Output file name
itself.png
